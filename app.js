// Require the functionality we need to use:
var http = require('http'),
	url = require('url'),
	path = require('path'),
	mime = require('mime'),
	path = require('path'),
	fs = require('fs');

var players = new Array();
var available = new Array();
var sockets = new Array();
var points = new Array();
 
// Make a simple fileserver for all of our static content.
// Everything underneath <STATIC DIRECTORY NAME> will be served.
var app = http.createServer(function(req, resp){
	var filename = path.join(__dirname, "static", url.parse(req.url).pathname);
	(fs.exists || path.exists)(filename, function(exists){
		if (exists) {
			fs.readFile(filename, function(err, data){
				if (err) {
					// File exists but is not readable (permissions issue?)
					resp.writeHead(500, {
						"Content-Type": "text/plain"
					});
					resp.write("Internal server error: could not read file");
					resp.end();
					return;
				}
 
				// File exists and is readable
				var mimetype = mime.lookup(filename);
				resp.writeHead(200, {
					"Content-Type": mimetype
				});
				resp.write(data);
				resp.end();
				return;
			});
		}else{
			// File does not exist
			resp.writeHead(404, {
				"Content-Type": "text/plain"
			});
			resp.write("Requested file not found: "+filename);
			resp.end();
			return;
		}
	});
//	socket.on("newplayer", function(name, fn) {
//		players.push(data.value);
//		fn(players.indexOf(name));
//		socket.broadcast.emit("updateplayers", players);
//	});
});
app.listen(3456);
var io = require('socket.io').listen(app);
io.sockets.on("connection", function(socket) {
	socket.emit("updateplayers", {
		players:players,
		available:available
	});
	socket.on("newplayer", function(name, fn) {
		if(players.indexOf(name) == -1) {
                	players.push(name);
			available.push(true);
			points.push(5);
			sockets.push(socket);
                	fn(players.indexOf(name));
                	socket.broadcast.emit("updateplayers", {
				players:players,
				available:available
			});
		} else {
			socket.emit("nametaken");
		}
        });

	socket.on("startgame", function(socket) {
		var initAngle = -60 + 120*Math.random();
		var initDirection = Math.random() < .5 ? -1 : 1;
		document.getElementById("gameContainer").style.display = "block";

		pong.init();
		pong.resetBall();
		pong.launch(initAngle, initDirection);
		socket.emit("launch", {
			angle: initAngle,
			direction: initDirection
		});
	});

	socket.on("challenge", function(data) {
		var playerIndex = players.indexOf(data.user);
		var opIndex = players.indexOf(data.op);
		if(opIndex == -1 || !available[opIndex]) {
			socket.emit("tryagain");
		} else {
			sockets[playerIndex].opponent = sockets[opIndex];
			sockets[playerIndex].opponent.emit("challenge", {
				user:data.user,
				op:data.op,
				pt:data.pt
			});
		}
	});
	socket.on("chalresp", function(data) {
		var playerIndex = players.indexOf(data.user);
		var opIndex = players.indexOf(data.op);
		if(data.check) {
			
			var initAngle = -60 + 120*Math.random();
			var initDirection = Math.random() < .5 ? -1 : 1;

			sockets[opIndex].opponent = sockets[playerIndex];
			available[playerIndex] = false;
			available[opIndex] = false;
			points[playerIndex] = data.pt;
                        points[opIndex] = data.pt;
			
			sockets[playerIndex].broadcast.emit("updateplayers", {
				players:players,
				available:available
			});
			sockets[opIndex].broadcast.emit("updateplayers", {
                                players:players,
                                available:available
                        });

			var score = {
				left: 0,
				right: 0
			};
			
			socket.emit("launch", {
				angle: initAngle,
				direction: initDirection
			}, score);
			socket.opponent.emit("launch", {
				angle: initAngle,
				direction: -initDirection
			}, score);
		} else {
			sockets[playerIndex].opponent = null;
		}
	});

	socket.on("reflect", function(vector) {
		socket.opponent.emit("reflect", vector);
	});

	socket.on("paddlemoved", function(position) {
		socket.opponent.emit("paddlemoved", position);
	});
	
	socket.on("pointscored", function(data) {
		var temp = data.score;
		var index = players.indexOf(data.user);
		socket.opponent.emit("pointscored", data.score);
		//delay for like a second or whatever
		var score = data.score;
		if(score.left < points[index] && score.right < points[index]) {
			var initAngle = -60 + 120*Math.random();
			var initDirection = Math.random() < .5 ? -1 : 1;
			console.log(points[index] + " " + score.left);
			socket.emit("relaunch", {
				angle: initAngle,
				direction: initDirection
			});
			socket.opponent.emit("relaunch", {
				angle: initAngle,
				direction: -initDirection
			});
		} else {
			
			socket.emit("gameover");
			socket.opponent.emit("gameover");
		}
	});

	socket.on("returntolobby", function(name) {
		var index = players.indexOf(name);
		available[index] = true;
		sockets[index].opponent = null;
		points[index] = 0;
		socket.broadcast.emit("updateplayers", {
			players: players,
			available:available
		});
	});

});

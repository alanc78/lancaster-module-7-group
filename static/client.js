Ext.DomHelper.useDom = true; // prevent XSS

var socket = io.connect(window.location.origin);
var playerid, username, score, pts, rally;

Ext.onReady(function() {
	console.log("document ready");
	
	Ext.fly("register").on("click", function(){
		username = Ext.fly("username").getValue();
		
		socket.emit("newplayer", username, function(data) {
			playerid = data;
			Ext.get("login").dom.innerHTML = "Logged in as " + username;
		});
	});
	socket.on("nametaken", function() {
		alert("Username taken. Try again.");
	});

	socket.on("updateplayers", function(data) { // Update list of players (login/logout)
		document.getElementById("users").innerHTML = ' ';
		for (var i = 0; i < data.players.length; i++) {
			var t = data.players[i];
			if(username != data.players[i] && data.available[i])
				Ext.fly("users").createChild({
					tag: "li",
					children: t
				});
		}
	});

	Ext.fly("gamerequest").on("click", function() {
		var opponent = Ext.fly("challenge").getValue();
		var points = Ext.fly("points").getValue();
		pts = points;
		socket.emit("challenge", {
			user:username,
			op:opponent,
			pt:points
		});
	});

	socket.on("challenge", function(data) {
		var check = confirm("You have been challenged by " + data.user + " to play to " + data.pt +" points. Would you like to accept?");
		pts = data.pt;
		socket.emit("chalresp", {
			check:check,
			user:data.user,
			op:data.op,
			pt:data.pt
		});
	});

	socket.on("launch", function(vector, zeroscore) {
		document.getElementById("gameContainer").style.display = "block";
		score = zeroscore;
		pong.init();
		pong.resetBall();
		pong.setScore(score);
		pong.launch(vector.angle, vector.direction);
		rally = 0;
		Ext.get("rally").dom.innerHTML = 'Rally: ' + rally;
	});

	window.addEventListener("paddlehit-left", function(e){
		if (e.detail.hit) {
			rally+=1;
			Ext.get("rally").dom.innerHTML = 'Rally: ' + rally;
			console.log(rally);
			socket.emit("reflect", {
				angle: e.detail.angle,
				position: e.detail.position,
			});
				
		}else{
		// in here, we will update the score, check for victory condition, launch a new ball (perhaps after a timeout), etc.
			score.right += 1;
			pong.setScore(score);
			socket.emit("pointscored", {
				user:username,
				score:score
			});
		}
	});

	socket.on("pointscored", function() {
		score.left += 1;
		pong.setScore(score);
	});

	window.addEventListener("paddlemove", function(e){
		socket.emit("paddlemoved", e.detail.position);
	});

	socket.on("relaunch", function(vector) {
		pong.resetBall();
		pong.launch(vector.angle, vector.direction);
		rally = 0;
		Ext.get("rally").dom.innerHTML = 'Rally: ' + rally;
	});

	socket.on("paddlemoved", function(position) {
		pong.updateOpponentPaddle(position);
	});

	socket.on("reflect", function(vector) {
		pong.resetBall(960, vector.position);
		pong.launch(vector.angle, -1);
		rally+=1;
		Ext.get("rally").dom.innerHTML = 'Rally: ' + rally;
		console.log(rally);
	});

	socket.on("gameover", function() {
		console.log(pts + " " + score.left);
		if(score.left == pts) {
			alert("You Win!");
		} else {
			alert("You lose...");
		}
		document.getElementById("gameContainer").style.display = "none";
		Ext.get("rally").dom.innerHTML = 'Rally: ' + rally;
		socket.emit("returntolobby", username);
	});
	
});
